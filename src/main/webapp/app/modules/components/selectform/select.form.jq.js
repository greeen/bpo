(function () {
    'use strict';

    function SelectTemplateJqService() {

        this.initBoxLoad = function(){
            delAll(); 
            init2();
            $('.leftmenu').css('left', '-330px');
        };

        this.loadJQ = function () {
            $('.leftmenu').css('left', '0px');


            $('.leftMenuHam p').click(function () {
                $('.leftmenu').css('left', '0px');
            });

            $('.leftHead').click(function () {
                $('.leftmenu').css('left', '-330px');
            });

            $('.leftmenu').on('click', '.leftMenuPage', function() {
                $('.leftmenu').css('left', '-330px');                
            });

            $('.selectFormcollapsibleOuter').collapsible({
                accordion: false
            });

            $('.selectFormcollapsibleInner').collapsible({
                accordion: false
            });

            $('.tmp-img').click(function () {
                $('#canvas2').css('background', 'url("assets/img/canvas.jpg") repeat scroll 0 0 rgba(0, 0, 0, 0)');
                init2();
            });

            $('body').on('click', 'a.firstLi', function() {
                console.log('in menu');
                $(this).find('ul').css({
                    'max-height': '500px',
                    'transition': '0.5s ease-in'
                });
            });

            $('.tmp-img2').click(function () {
                console.log('in second');
                $('#canvas2').css('background', 'url("assets/img/canvas.png") repeat scroll 0 0 rgba(0, 0, 0, 0)');
                init2();
            });
        };
        return this;
    }


    SelectTemplateJqService.$inject = ['alertService'];

    angular
        .module('app.modules.components.selectForm.jq.service', [])
        .service('selectTemplateJqService', SelectTemplateJqService);

})();