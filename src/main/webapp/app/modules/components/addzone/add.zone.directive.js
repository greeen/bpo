(function () {
  'use strict';

  function AddZoneController($scope, clipTypeEnum, addZoneJqService, CLIP_TYPE_DESC, QUALITY_TYPE, SLA, LANGUAGE, alertService) {

    var templatesModel = $scope.templatesModel;
    var parentCtrl = $scope.$parent.templatesCtrl;

    this.vm = templatesModel.markedZone;
    this.CLIP_TYPE_DESC = CLIP_TYPE_DESC;
    this.QUALITY_TYPE = QUALITY_TYPE;
    this.SLA = SLA;
    this.LANGUAGE = LANGUAGE;

    this.alertfn = function () {
      addZoneJqService.loadBoxJQ(templatesModel.template.markedZoneArray);
    };

    this.closeModal = function () {
      $('#editModal').closeModal();
    };

    this.duplicateCheck = function(fieldName){
      $.each(templatesModel.template.markedZoneArray, function(index, val) {
        if(val.CLIP_FIELD_NAME == fieldName){
          alertService.warning('Duplicate field name <b><i>"' + fieldName + '"</b></i> entered !! <br> Please enter a <b>Unique</b> field name.');
          templatesModel.markedZone.CLIP_FIELD_NAME = '';
        }
      });
    }

    this.addZone = function () {
      /** box_cords is global object */
      var markedZone = this.vm;
      // feed cordinates
      // addition of table field for tableUI

      //addZoneJqService.feedCordsVar();
      if (!(templatesModel.allowZoneMarking)) {
        markedZone.CLIP_TYPE = clipTypeEnum.NUMBER;
        markedZone.CLIP_FIELD_NAME = 'form_no';
        addZoneJqService.feedCordsVar();

      }else{
        if ( markedZone.CLIP_TYPE === 'MST'){
          addZoneJqService.feedCordsTableSingle();
        }else if (markedZone.CLIP_TYPE === 'MST') {
          addZoneJqService.feedCordsTableMultiple();
        }else{
          addZoneJqService.feedCordsVar();
        }    
      }
      //addZoneJqService.feedCordsVar();

      markedZone.CLIP_COORDINATE_X1 = box_cords.x1;
      markedZone.CLIP_COORDINATE_X2 = box_cords.x2;
      markedZone.CLIP_COORDINATE_Y1 = box_cords.y1;
      markedZone.CLIP_COORDINATE_Y2 = box_cords.y2;

      if( (markedZone.CLIP_COORDINATE_X1 < 0) || (markedZone.CLIP_COORDINATE_Y1 < 0) || (markedZone.CLIP_COORDINATE_X2 > templatesModel.template.RES_IMAGE_WIDTH) || (markedZone.CLIP_COORDINATE_Y2 > templatesModel.template.RES_IMAGE_HEIGHT)){
        alertService.warning('Zone marked <b>Out of boundary</b> !!<br> Please mark the zone <b>within the boundary</b>');
        return;
      }
      //validate MarkedZone
      if (validateVmFields(this.vm) === false) {
        return;
      }
      //push into zone array and clear current zone
      templatesModel.pushMarkedZone();
      templatesModel.clearMarkedZone();
      templatesModel.allowZoneMarking = true;
      templatesModel.allowFieldInput = false;
    };

    this.editZone = function () {
      if (templatesModel.template.markedZoneArray.length > 1) {
        templatesModel.template.markedZoneArray.sort(function (item1, item2) {
          return item1.CLIP_SEQ_NO - item2.CLIP_SEQ_NO;
        });
      }
      this.markedZoneArray = templatesModel.template.markedZoneArray;
      var misSeqNo = absent(this.markedZoneArray);
      if (misSeqNo.length > 0) {

      alertService.warning('Missing Clip Sequence No  : ' + misSeqNo);
    }
    };

    this.saveZone = function () {
      var obj = templatesModel.template.markedZoneArray[0];
      if ((obj.CLIP_FIELD_NAME === 'form_no') && (obj.CLIP_TYPE === 'NM') && (obj.CLIP_SEQ_NO === 1)) {
        parentCtrl.saveROI();
      } else {
        alertService.alert('Please enter form_no as first field.', 'Mandatory');
      }
    };

    this.onDeleteModal = function () {
      //console.log('in del',index);
      for (var i = 0; i < templatesModel.template.markedZoneArray.length; i++) {
        templatesModel.template.markedZoneArray[i].CLIP_SEQ_NO = i + 1;
      }
    };

    function validateVmFields(vm) {
      if (!templatesModel.allowFieldInput) {
        alertService.alert('Draw the zone first', 'Mandatory');
        return false;
      }
      if (!vm.CLIP_COORDINATE_X1 || !vm.CLIP_COORDINATE_X2) {
        alertService.alert('Draw the zone.', 'Mandatory');
        return false;
      }
      if (!vm.CLIP_TYPE) {
        alertService.alert('Type  value is missing.', 'Mandatory');
        return false;
      }
      if (!vm.CLIP_FIELD_NAME) {
        alertService.alert('Field Name value is missing.', 'Mandatory');
        return false;
      }
    }

    function absent(markedZones){
      var arr = [];
      markedZones.forEach(function(item){
        arr.push(item.CLIP_SEQ_NO);
      });

      var mia= [], min= Math.min.apply('',arr), max= Math.max.apply('',arr);
      while(min<max){
        if(arr.indexOf(++min)== -1) mia.push(min);
      }
      return mia;
    }

  } //end of Controller

  /** Directive Class */
  function AddZoneDirective(addZoneJqService) {
    var directive = {};
    directive.init = function () {
      addZoneJqService.loadJQ();
    };
    directive.controller = ['$scope', 'clipTypeEnum', 'addZoneJqService', 'CLIP_TYPE_DESC', 'QUALITY_TYPE', 'SLA', 'LANGUAGE', 'alertService', AddZoneController];
    directive.controllerAs = 'addZoneCtrl';
    directive.scope = {
      templatesModel: "=ngModel"
    };
    directive.restrict = 'EA';
    /* restrict this directive to elements */
    directive.templateUrl = "app/modules/components/addzone/add.zone.view.html";
    directive.link = function ($scope, element, attrs) {
      directive.init();
    };
    return directive;
  }

  AddZoneDirective.$inject = ['addZoneJqService', 'CLIP_TYPE_DESC', 'QUALITY_TYPE', 'SLA', 'LANGUAGE', 'alertService'];
  angular
    .module('app.modules.components.addZone', ['app.modules.components.addZone.jq.service'])
    .directive('addZone', AddZoneDirective);
})();