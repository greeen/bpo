(function () {

///////////////////////////////////////////////////////////////////////////
// NOTE: Has to be on a type="text" input so that Angular's              //
// NgModelController $parser triggers for all text                       //
// If it's on a Number input, text (e.g. e), decimel etc doesn't trigger //
// Also, ng-trim has to be false so that function added to $parser       //
// triggers for whitespace                                               //
///////////////////////////////////////////////////////////////////////////
angular.module('earngoApp').directive('digitsOnly', function () {
    var linker = function (scope, element, attrs, ngModelCtrl) {
        // Declare function that prevents non digit inputs
        function preventNondigitValues (inputValue) {
            if (inputValue === undefined) return '';
            var transformedInput = inputValue.replace(/[^0-9]/g, '');
            if (transformedInput !== inputValue) {
                ngModelCtrl.$setViewValue(transformedInput);
                ngModelCtrl.$render();
            }
            return transformedInput;
        }

        // Add the filter to the model controller, which provides API for
        // the ngModel directive
        // https://docs.angularjs.org/api/ng/type/ngModel.NgModelController
        ngModelCtrl.$parsers.push(preventNondigitValues);
    };

    return {
        restrict: 'A',
        require: 'ngModel',
        link: linker
    };
});

})();