(function () {
    'use strict';

    function TemplatesJqService(alertService) {

        this.load = function () {
/*            $(window).bind('beforeunload', function(){
              return '';
            });*/

            $('.nav-sliding-middle-out').click(function() {
                $('.nav-sliding-middle-out').css('border-bottom', '0');
                $(this).css('border-bottom', '2px solid white');
            });

            // $('.tmpBtnModal').click(function() {
            //     $('#tmp-modal').openModal();
            // });

            $('#canvas2').dblclick(function() {
                $('.rightmenu').css('right', '0px');
            });

            $('.tooltipped').tooltip({delay: 50});

            $('body').on('click', 'input[name="typeMcqRad"]', function() {
                // addition of table field for tableUI
                if($(this).val() == 'MST'){
                    $('#choices_number_Single_col').show();
                    $('#choices_number_Single_row').show();
                    $('#choices_number_Single_normal').hide();
                }else if(($(this).val() == 'single_select_horizontal') || ($(this).val() == 'single_select_vertical')){
                    $('#choices_number_Single_normal').show();
                    $('#choices_number_Single_col').hide();
                    $('#choices_number_Single_row').hide();
                    // addition of table field for tableUI
                }else if($(this).val() == 'MMT'){
                    $('#choices_number_Mul_row').show();
                    $('#choices_number_Mul_col').show();
                    $('#choices_number_Mul_normal').hide();
                }else if(($(this).val() == 'multi_select_horizontal') || ($(this).val() == 'multi_select_vertical')){
                    $('#choices_number_Mul_normal').show();
                    $('#choices_number_Mul_row').hide();
                    $('#choices_number_Mul_col').hide();
                }else{
                    $('#choices_number_Single_normal').hide();
                    $('#choices_number_Single_col').hide();
                    $('#choices_number_Single_row').hide();
                    $('#choices_number_Mul_normal').hide();
                    $('#choices_number_Mul_row').hide();
                    $('#choices_number_Mul_col').hide();
                }
            });

            $('.tmpBtnBox').click(function() {
                showLastCoord();
                $('.tmp-hide-modal-boxcordx1').val(box_cords.x1);
                $('.tmp-hide-modal-boxcordy1').val(box_cords.y1);
                $('.tmp-hide-modal-boxcordx2').val(box_cords.x2);
                $('.tmp-hide-modal-boxcordy2').val(box_cords.y2);
            });

            $('#tmp-del-rec').click(function() {
                delRec();
            });

            $('body').on('click', '.clipMulBtnAgree',function(){
                var val_type = $('input[name="typeMcqRad"]:checked').val();
                switch (val_type) {
                    // addition of table field for tableUI
                    case 'MST':
                        var rows = $('body').find('#choices_number_Single_row').val();
                        var cols = $('body').find('#choices_number_Single_col').val();
                        mulRec(rows, cols);
                        break;
                    case 'single_select_horizontal':
                        var choice = $('body').find('#choices_number_Single_normal').val();
                        mulRec(1, choice);
                        break;
                    case 'single_select_vertical':
                        var choice = $('body').find('#choices_number_Single_normal').val();
                        mulRec(choice, 1);
                        break;
                    // addition of table field for tableUI
                    case 'MMT':
                        var rows = $('body').find('#choices_number_Mul_row').val();
                        var cols = $('body').find('#choices_number_Mul_col').val();
                        mulRec(rows, cols);
                        break;
                    case 'multi_select_horizontal':
                        var choice = $('body').find('#choices_number_Mul_normal').val();
                        mulRec(1, choice);
                        break;
                    case 'multi_select_vertical':
                        var choice = $('body').find('#choices_number_Mul_normal').val();
                        mulRec(choice, 1);
                        break;
                }

            });



            };
        return this;
    }


    TemplatesJqService.$inject = ['alertService'];

    angular
        .module('app.modules.templates.jq.service', [])
        .service('templatesJqService', TemplatesJqService);

})();