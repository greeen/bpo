/**
 * Created by Rajesh on 16/5/16.
 */
(function () {
  'use strict';

  function TemplatesService(restService, config, addZoneJqService, clipTypeEnum, alertService) {


    var service = {};

    service.saveROI = function (model) {

      var saveRoiReq = {
        CUST_SRNO: model.customerId,
        TEMPLATE_ID: model.template.TEMPLATE_ID,
        FORM_ID: model.template.FORM_ID,
        FORM_NAME: model.template.FORM_NAME,
        MP: model.template.MP,
        PAGE_LINKAGE: model.template.PAGE_LINKAGE,
        RES_IMAGE_HEIGHT: model.template.RES_IMAGE_HEIGHT,
        RES_IMAGE_PATH: model.template.RES_IMAGE_PATH,
        RES_IMAGE_WIDTH: model.template.RES_IMAGE_WIDTH,
        //TOTAL_ZONE_MARKINGS: model.template.TOTAL_ZONE_MARKINGS,
        NO_OF_FIELDS:model.template.markedZoneArray.length,
        array: model.template.markedZoneArray
      };

      var promise = restService.httpPostPromise(config.SAVE_TEMPLATE_ROI, saveRoiReq)
        .then(function (result) {
          console.log('ROI Result', result.RES_STATUS);
          if (result.RES_STATUS === 'S') {
            alertService.alert('Template Saved.', 'Confirmation');
          }
        });
      return promise;
    };

    service.fetchROI = function (templatePage, model) {
      var roiReq = {
        CUST_SRNO: model.customerId,
        TEMPLATE_ID: templatePage.pageId
      };
      model.setDefaultMarkedZone();
      var promise = restService.httpPostPromise(config.FETCH_TEMPLATE_ROI, roiReq)
        .then(function (result) {
          if (result.RES_STATUS === 'S') {
            model.template.TEMPLATE_ID = result.TEMPLATE_ID || roiReq.TEMPLATE_ID;
            model.template.FORM_ID = result.FORM_ID;
            model.template.FORM_NAME = result.FORM_NAME;
            model.template.MP = result.MP;
            model.template.PAGE_LINKAGE = result.PAGE_LINKAGE;
            model.template.RES_IMAGE_HEIGHT = result.RES_IMAGE_HEIGHT;
            model.template.RES_IMAGE_PATH = result.RES_IMAGE_PATH;
            model.template.RES_IMAGE_WIDTH = result.RES_IMAGE_WIDTH;
            model.template.TOTAL_ZONE_MARKINGS = result.TOTAL_ZONE_MARKINGS;
            model.template.markedZoneArray = [].concat(result.array);
            addZoneJqService.loadBoxJQ(model.template.markedZoneArray);
            if (model.template.markedZoneArray.length > 0) {
              model.allowZoneMarking = true;
            } else {
              model.allowZoneMarking = false;
              alertService.alert('Please make sure you mark the <b>FORM NUMBER</b> field first !');
            }

            if (model.template.markedZoneArray && model.template.markedZoneArray.length === 0) {
              model.markedZone.CLIP_FIELD_NAME = 'form_no'; //set default to form_no
              model.markedZone.CLIP_TYPE = clipTypeEnum.NUMBER;
              model.markedZone.CLIP_MANDATORY = 'Y';
              model.markedZone.CLIP_SEQ_NO = 1;
              model.markedZone.CLIP_SUB_SEQ_NO = 1;
            } else {
              model.markedZone.CLIP_SEQ_NO = model.template.markedZoneArray.length + 1;
            }
          }
        });
      return promise;
    };

    service.fetchForms = function (model) {
      var fetchFormReq = {
        CUST_SRNO: model.customerId
      };

      var promise = restService.httpPostPromise(config.FETCH_ALL_TEMPLATE_DETAIL, fetchFormReq)
        .then(function (result) {
          console.log(result.FORM_DETAILS);
          model.templateList = result.FORM_DETAILS;
        });
      return promise;
    };

    return service;
  }

  TemplatesService.$inject = ['restService', 'config', 'addZoneJqService', 'clipTypeEnum', 'alertService'];

  angular
    .module('app.modules.templates.service', [])
    .service('templatesService', TemplatesService);

})();