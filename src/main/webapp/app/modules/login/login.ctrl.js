(function () {
  'use strict';


  function LoginController(loginModel, loginService) {

    var ctrl = this;
    ctrl.vm = loginModel;

    ctrl.login = function () {
      loginService.login(loginModel);
      //$location.path('/main');
    };

    ctrl.init = function () {
      loginService.deviceInit();
    };
  }

  LoginController.$inject = ['loginModel', 'loginService'];

  angular
    .module('app.modules.login', ['app.modules.login.service'])
    .controller('loginController', LoginController);

})();