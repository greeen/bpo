/**
 *  This file is NOT IN USE anymore and Need to Remove Later
 */

var clipTypeEnum = {
    SHORT_TEXT: "ST",
    LOGN_TEXT: "LT",
    SINGLE_SELECT_VERT: "MSV", // RADIO BUTTON
    SINGLE_SELECT_HOR: "MSH", // RADIO BUTTON
    MULTI_SELECT_VERT: "MMV", //Checkbox
    MULTI_SELECT_HOR: "MMH", //check boxes
    PHONE_NUMBER: "PH",
    NUMBER: "NM",
    DATE: "DT",
    TIME: "TM",
    DATE_AND_TIME: "DM",
    SNS: "SNS", // RADIO BUTTON
    MALE_FEMALE: "MF", // RADIO BUTTON
    YES_NO: "YN", // RADIO BUTTON
    LARGE_CLIP: "XL", //text area
    LARGE_MCQ: "XLMMV", //checkbox
};

var htmlInputType = {
    RAD: "RADIO",
    CHK: "CHECKBOX",
    TXT: "TEXTBOX",
};


// Class Object for GetNextImage
function GetNextImage(clipNo, clipType, clipPath, customMsg) {
    this.clipNo = clipNo;
    this.clipType = clipType;
    this.clipPath = clipPath;
    this.clipHtmlInputType = getHtmlInputType(clipType);
    this.clipHtmlId = this.clipHtmlInputType + "_" + clipNo;
    this.clipHtmlName = "name_" + this.clipHtmlInputType + "_" + clipNo;
    this.clipAngularId = "ng_" + this.clipHtmlInputType + "_" + clipNo;
    this.clipUnclearCheckId = "chkBoxUnclear" + "_" + clipNo;
    this.clipValue = "";
    this.isClipValueExtracted = false;
    this.customMsg = customMsg;
    this.defaultMsg = "";
    this.clipImgId="img_"+clipNo ;

    //date & time fields
    // below special case during Date&Time field, used for Time
    this.clipHtmlId2 = this.clipHtmlInputType + "2_" + clipNo;
    //below fields needs to hold the Array of Data of Checker Value
    this.checkerDataArr=null;
}


// class for Checkerdatavalue
GetCheckerData= function(distNo,outTrayInput,outTrayAction){
    this.distNo=distNo;
    this.outTrayInput=outTrayInput;
    this.outTrayAction=outTrayAction;
    this.htmlId="id_"+distNo;
};

/**
 * convert DataObject which comes from REST, into JS object with needed aditional fields
 */
function convertToJsObject(dataObject) {
    var arr = [];
    if (dataObject) {
        dataObject.forEach(function (item) {
            var getNextObj = new GetNextImage(item.clipNo, item.clipType, item.clipPath, item.customMsg);
            //arr.push(new GetNextImage(item.clipNo, item.clipType, item.clipPath, item.customMsg));

            //retrieve data for checkerData value
            // NOTE: eval may be dangerous if the argument supplied cannot be trusted!!!
            // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/eval#Don't_use_eval_needlessly!
            var chkArrData = eval( item.checkerDataArr  );

            if(chkArrData===undefined || chkArrData == null ){
                //do nothing
                getNextObj.checkerDataArr=null;
            }else{
                var checkerArr=[];
                for (var j = 0; j < chkArrData.length; j++) {
                    //Res :[{"DISTRIBUTOR_NUM":1420,"OUTTRAY_INPUT":"*","OUTTRAY_ACTION":1},{"DISTRIBUTOR_NUM":236,"OUTTRAY_INPUT":"*","OUTTRAY_ACTION":2}]
                    var chkObj= new GetCheckerData(chkArrData[j].DISTRIBUTOR_NUM,chkArrData[j].OUTTRAY_INPUT,chkArrData[j].OUTTRAY_ACTION);
                    checkerArr[j]=chkObj;
                }

                getNextObj.checkerDataArr=checkerArr;
            }
            arr.push(getNextObj);
        });
    }
    return arr;
}

function getHtmlInputType(clipType) {
    //for Textbox
    if (clipType === clipTypeEnum.SHORT_TEXT || clipType == clipTypeEnum.LOGN_TEXT || clipType == clipTypeEnum.LOGN_TEXT || clipType == clipTypeEnum.PHONE_NUMBER || clipType == clipTypeEnum.NUMBER || clipType == clipTypeEnum.DATE || clipType == clipTypeEnum.TIME || clipType == clipTypeEnum.DATE_AND_TIME || clipType == clipTypeEnum.LARGE_CLIP) {
        return htmlInputType.TXT;
    }
    //for Radio button
    if (clipType == clipTypeEnum.SINGLE_SELECT_VERT || clipType == clipTypeEnum.SINGLE_SELECT_HOR || clipType == clipTypeEnum.SNS || clipType == clipTypeEnum.MALE_FEMALE || clipType == clipTypeEnum.YES_NO) {
        return htmlInputType.RAD;
    }
    //for checkbox
    if (clipType == clipTypeEnum.MULTI_SELECT_VERT || clipType == clipTypeEnum.MULTI_SELECT_HOR || clipType == clipTypeEnum.LARGE_MCQ) {
        return htmlInputType.CHK;
    }
}


function retrieveDataFromInputHtmlType(gNxtObjArr, dataForCheckerCb) {

    for (var i = 0; i < gNxtObjArr.length; i++) {
        // console.log("Enter the data : " + JSON.stringify(gNxtObjArr[i]));
        var inputVal = "";
        var checkerFieldValue = null;

        if(dataForCheckerCb){
            checkerFieldValue = dataForCheckerCb(gNxtObjArr[i].checkerDataArr);
        }

        //get unclear Checkbox Value if selected.  clipUnclearCheckId
        var unclearChkValue = $('input[name=' + gNxtObjArr[i].clipUnclearCheckId + ']:checked').val();
        if (unclearChkValue === "UN") {
            //unclear checkbox value
            inputVal = '*';
        } else if (checkerFieldValue != null) {
            inputVal = checkerFieldValue;
        }else if (gNxtObjArr[i].clipHtmlInputType == htmlInputType.CHK) {
            //if input type is checkbox
            $('input[name=' + gNxtObjArr[i].clipHtmlName + ']:checked').each(function () {
                // console.log("checked value :" + this.value);
                inputVal = inputVal + this.value + ",";
            });
            if (inputVal != null || inputVal != "") {
                // remove last char ,
                inputVal = inputVal.substring(0, inputVal.length - 1);
            }
        } else if (gNxtObjArr[i].clipHtmlInputType == htmlInputType.RAD) {
            //if input type is Radio Button
            inputVal = $('input[name=' + gNxtObjArr[i].clipHtmlName + ']:radio:checked').val();
        }
        else if (gNxtObjArr[i].clipHtmlInputType == htmlInputType.TXT && gNxtObjArr[i].clipType == "NM") {
            // if input Type is Textbox or Textarea
            var value = $('#' + gNxtObjArr[i].clipHtmlId).val();
            inputVal = Number(value);
            // console.log("Number value = " + inputVal);
        } else if (gNxtObjArr[i].clipHtmlInputType == htmlInputType.TXT) {
            // if input Type is Textbox or Textarea
            inputVal = $('#' + gNxtObjArr[i].clipHtmlId).val();
        }

        // special case when Date&time field
        if (gNxtObjArr[i].clipType == clipTypeEnum.DATE_AND_TIME) {
            // if input Type is Textbox or Textarea
            var timeValue = $('#' + gNxtObjArr[i].clipHtmlId2).val();
            inputVal = inputVal + "" + timeValue;
        }

        // get the overflow value
        var overflowValue = $('#chkBoxOverflow_'+gNxtObjArr[i].clipNo).prop('checked');
        gNxtObjArr[i].overflow = overflowValue ? 1 : 0; // convert true / false to 1 / 0

        if (inputVal == null || inputVal == "") {
            showAlert("Please select the value for Row No : " + (i + 1));
            return false;
        }

        // console.log("Scope inputVal : " + JSON.stringify(inputVal));
        gNxtObjArr[i].clipValue = inputVal;

    } // end of loop

    return true;
};


function showAlert(msg) {
    $('#modal-alert').find('.modal-content p').text(msg);
    $('#modal-alert').openModal();
    $('#modal-alert-bttn-ok').focus();
}


function retrieveDataFromCheckerFields (checkerDataArr) {

    var checkedValue = '';
    var value = null;

    if (checkerDataArr === undefined || checkerDataArr == null || checkerDataArr === '') {
        return value;
    }
    for (var i = 0; i < checkerDataArr.length; i++) {
        checkedValue = $('input[name=' + checkerDataArr[i].htmlId + ']:checked').val();

        if (checkedValue) {
            value = checkedValue;
        }
    }
    return value;
};

