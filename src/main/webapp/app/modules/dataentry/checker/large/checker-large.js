(function () {
    'use strict';

    function CheckerLargeEntryController(checkerEntryCtrlFactory, config) {
        var meCtrl = checkerEntryCtrlFactory.getInstance(config.CHK_LARGE_DATA_AFTER_SUBMIT, config.CHK_LARGE_DATA_ON_PAGE_LOAD);
        return meCtrl;
    }


    CheckerLargeEntryController.$inject = ['checkerEntryCtrlFactory', 'config'];
    angular.module('app.modules.dataEntry.checker.large', ['ui.router', 'app.earngo.mock.backend', 'app.modules.dataentry.checker'])
        .controller('checkerLargeEntryController', CheckerLargeEntryController);

})();