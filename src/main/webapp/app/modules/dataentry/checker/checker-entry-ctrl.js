(function () {
    'use strict';

    function CheckerEntryCtrlFactory($rootScope, restService, config, dataEntryUtilService) {

        /**
         * Checker Entry Controller JS Class
         * */
        function CheckerEntryController() {
            var ctrl = this;

            ctrl.clipDataVm = {
                clipsSetOne: null,
                clipsSetTwo: null,
                showDivClipEntryOne: false,
                showDivClipEntryTwo: false,
                disableDivClipEntryOne: false,
                disableDivClipEntryTwo: false,
                divOne: { // div 1 model
                    loading: false
                },
                divTwo: { // div 2 model
                    loading: false
                }
            };
            var clipData = ctrl.clipDataVm;

            // get all tasks and display initially
            ctrl.loadDataWithController = function loadDataWithController() {

                clipData.disableDivClipEntryOne = false;
                clipData.disableDivClipEntryTwo = false;
                clipData.showDivClipEntryOne = true;
                clipData.showDivClipEntryTwo = false;
                restService.httpPostPromise(IMAGE_ON_PAGE_LOAD_URL, null)
                    .then(function (jsonArr) {
                        // pass this info to scope so that inputs do not need
                        // to set the html names etc separately, more maintainable
                        clipData.clipsSetOne = dataEntryUtilService.convertToJsObject(jsonArr.imagesSetOne);
                        clipData.clipsSetTwo = dataEntryUtilService.convertToJsObject(jsonArr.imagesSetTwo);
                    });
            };

            // Below method submit and populate data for DIV One
            ctrl.dataSubmit_One = function dataSubmit_One() {
                var gNxtObjArr = clipData.clipsSetOne;
                // extract data and post into Earngo
                var isAllDataEntered = dataEntryUtilService.retrieveDataFromInputHtmlType(gNxtObjArr, dataEntryUtilService.retrieveDataFromCheckerFields);
                if (isAllDataEntered === false) {
                    return;
                }
                // data is valid, reset timer by broadcasting RESET_TIMER event
                $rootScope.$broadcast('RESET_TIMER');
                clipData.disableDivClipEntryOne = true;
                clipData.divOne.loading = true;
                saveAndFetchData(gNxtObjArr).then(function (clipsSetOne) {
                    clipData.clipsSetOne = clipsSetOne;
                    clipData.divOne.loading = false;
                });
                clipData.disableDivClipEntryTwo = false;
                clipData.showDivClipEntryOne = false;
                clipData.showDivClipEntryTwo = true;
                $('#divClipEntryTwo input').not('.unclear').first().focus();
            };

            // Below method submit and poupulate data for DIV Two
            ctrl.dataSubmit_Two = function dataSubmit_Two() {

                var gNxtObjArr = clipData.clipsSetTwo;
                // extract data and post into Earngo
                var isAllDataEntered = dataEntryUtilService.retrieveDataFromInputHtmlType(gNxtObjArr, dataEntryUtilService.retrieveDataFromCheckerFields);
                if (isAllDataEntered == false) {
                    return;
                }
                // data is valid, reset timer by broadcasting RESET_TIMER event
                $rootScope.$broadcast('RESET_TIMER');

                // extract data and post into Earngo
                clipData.disableDivClipEntryTwo = true;
                clipData.divTwo.loading = true;

                saveAndFetchData(gNxtObjArr).then(function (clipsSetTwo) {
                    clipData.clipsSetTwo = clipsSetTwo;
                    clipData.divTwo.loading = false;
                });
                clipData.disableDivClipEntryOne = false;
                clipData.showDivClipEntryOne = true;
                clipData.showDivClipEntryTwo = false;
                //add set focus
                $('#divClipEntryOne input').not('.unclear').first().focus();
            };


            function saveAndFetchData(gNxtObjArr) {
                // submit the data
                restService.httpPostPromise(config.POST_IMAGE_RESULT, gNxtObjArr)
                    .then(function (data) {
                        console.log('saved', data);
                    });

                var promise = restService.httpPostPromise(NEXT_IMAGE_AFTER_SUBMIT_URL, null)
                    .then(function (jsonArr) {
                        return dataEntryUtilService.convertToJsObject(jsonArr.imagesSetOne);
                    });
                return promise;
            }
        }//CheckerEntryController

        var factory = {};
        var NEXT_IMAGE_AFTER_SUBMIT_URL = null;
        var IMAGE_ON_PAGE_LOAD_URL = null;

        factory.getInstance = function (nextImageAfterSubmitUrl, imageOnPageLoadUrl) {
            NEXT_IMAGE_AFTER_SUBMIT_URL = nextImageAfterSubmitUrl;
            IMAGE_ON_PAGE_LOAD_URL = imageOnPageLoadUrl;
            return new CheckerEntryController();
        };
        return factory;
    }

    CheckerEntryCtrlFactory.$inject = ['$rootScope', 'restService', 'config', 'dataEntryUtilService'];

    angular
        .module('app.modules.dataentry.checker', ['app.modules.dataentry.common.util'])
        .factory('checkerEntryCtrlFactory', CheckerEntryCtrlFactory);

})();