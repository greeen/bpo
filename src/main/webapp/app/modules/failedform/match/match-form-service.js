/**
 * Created by Rajesh on 5/4/16.
 */
(function (angular) {
    'use strict';

    //matchform View Model
    function MatchformModel() {
        this.unidentifiedFormPath = 'assets/img/no-image-available.png';
        this.unidentifiedFormId = null;
        this.custTemplatePath = 'assets/img/no-image-available.png';
        this.custTemplateId = null;
        this.custTemplateName = null;
        this.custAvailableTemplates = [];
        this.custSrNo = null;
        this.seqNo = null;
        this.templateId = null;
        this.isImageMatch = 'N';
        this.isFormSelected = false;
        this.statusLabel = 'UnIdentified form is not verified.';
    }

    MatchformModel.prototype.testfun = function () {
        alert('alert');
    };

    //new Form Popup Directive

    function NewTemplateModalDirective() {

        var earngoDefaults = {
            dismissible: false, // Modal can be dismissed by clicking outside of the modal
            opacity: 0.5, // Opacity of modal background
            in_duration: 300, // Transition in duration
            out_duration: 200, // Transition out duration
            ready: function () {
            },
            complete: function () {
            }
        };

        var linker = function (scope, element, attrs) {
            var options = $.extend(true, {}, earngoDefaults, scope.$eval(attrs.modal));
            var modalElement = $(element).find('#modal-help');
            // launch the modal when signal is received (e.g. on Help nav item click)
            //scope.$on('HELP_MODAL_LAUNCH', function (evt) {
            //   modalElement.openModal(options);
            //});
        };

        return {
            restrict: 'E',
            link: linker,
            templateUrl: 'app/modules/failedform/match/new-template-modal.html'
        };
    }

    //Matchform Data Service
    var mfDataService = function (restService, bpoConstants) {

        function init(matchformModel) {
            nextForm(matchformModel);
        }

        function saveMatchData(matchformModel) {
            matchformModel.isFormSelected = true;
            matchformModel.isImageMatch = 'Y';
            saveData(matchformModel);
            matchformModel.statusLabel = 'Unidentified form match found.';
        }

        function getNextForm(matchformModel) {
            nextForm(matchformModel);
        }

        function saveDiscardData(matchformModel) {
            matchformModel.isFormSelected = true;
            matchformModel.custTemplateId = 'DISCARD';
            matchformModel.isImageMatch = 'N';
            //save data
            saveData(matchformModel);
            matchformModel.statusLabel = 'Unidentified form discarded.';
        }

        function nextForm(matchformModel) {
            var randomNo = Math.random();
            restService.httpPostPromise(bpoConstants.NEXT_MATCH_FORM_DETAIL, {})
                .then(function (resData) {
                    if (resData && resData.RES_STATUS === 'S') {
                        setMatchFormModelData(resData, matchformModel);
                    }else{
                        setMatchFormAsDefault(matchformModel);
                    }
                });
        }

        function saveData(matchformModel) {
            var serverData = createServerModelData(matchformModel);

            restService.httpPostPromise(bpoConstants.POST_MATCH_FORM_DETAIL, serverData)
                .then(function (resData) {
                    console.log('saveMatchedData ');
                });
        }

        function setMatchFormModelData(serverData, matchformModel) {

            matchformModel.custSrNo = serverData.cust_srno;
            matchformModel.seqNo = serverData.SEQ_NO;
            matchformModel.unidentifiedFormId = serverData.unknown_image_id;
            matchformModel.unidentifiedFormPath = serverData.unknown_image_path;

            matchformModel.custAvailableTemplates = serverData.templateArray;
            matchformModel.custTemplatePath = matchformModel.custAvailableTemplates[0].template_path;
            matchformModel.custTemplateId = matchformModel.custAvailableTemplates[0].template_id;
            matchformModel.custTemplateName = matchformModel.custAvailableTemplates[0].template_name;

            matchformModel.subFormId = matchformModel.custAvailableTemplates[0].sub_form_id;
            matchformModel.formName = matchformModel.custAvailableTemplates[0].form_name;
            matchformModel.formId = matchformModel.custAvailableTemplates[0].form_id;
            matchformModel.isFormSelected = false;

        }

        function setMatchFormAsDefault(matchformModel){

            matchformModel.unidentifiedFormPath = 'img/no-image-available.png';
            matchformModel.unidentifiedFormId = null;
            matchformModel.custTemplatePath = 'img/no-image-available.png';
            matchformModel.custTemplateId = null;
            matchformModel.custTemplateName = null;
            matchformModel.custAvailableTemplates = [];
            matchformModel.custSrNo = null;
            matchformModel.seqNo = null;
            matchformModel.templateId = null;
            matchformModel.isImageMatch = 'N';
            matchformModel.isFormSelected = false;
            matchformModel.statusLabel = 'No form is available.';

        }

        function createServerModelData(matchformModel) {

            var serverData = {
                custNo: matchformModel.custSrNo,
                failedImageId: matchformModel.unidentifiedFormId,
                failedImageLinkTemplate: matchformModel.custTemplateId,
                failedImageLinkTemplateName: matchformModel.custTemplateName,
                isImageMatch: matchformModel.isImageMatch,
                seqNo: matchformModel.seqNo
            };
            return serverData;
        }

        function selectAvailableTemplate(custAvailTemplate, matchformModel) {
            if (!custAvailTemplate) {
                return;
            }
            matchformModel.custTemplatePath = custAvailTemplate.template_path;
            matchformModel.custTemplateId = custAvailTemplate.template_id;
            matchformModel.custTemplateName = custAvailTemplate.template_name;
            matchformModel.subFormId = custAvailTemplate.sub_form_id;
            matchformModel.formName = custAvailTemplate.form_name;
            matchformModel.formId = custAvailTemplate.form_id;
        }

        return {
            init: init,
            saveMatchData: saveMatchData,
            getNextForm: getNextForm,
            saveDiscardData: saveDiscardData,
            selectAvailableTemplate: selectAvailableTemplate
        };
    };

    var module = angular.module('app.services.matchform', []);
    module.service('matchformDataService', mfDataService);
    module.service('matchformModel', MatchformModel);
    module.directive('newTemplateModal', NewTemplateModalDirective);

})(angular);