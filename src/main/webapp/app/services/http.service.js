var module = angular.module('app.modules.rest.service', []);

/**
 * Common service created to handle HttpCalls and Add common parameter to headers
 */
module.service('restService', function ($http, $q, config, alertService) {
    "use strict";
    return {

        /*This method is only applicable for only once, during first page loading. */
        httpPostInit: function () {
            // Initialize the device
            var req = {
                method: "post",
                url: config.INITIALIZE_DEVICE,
                headers: {
                    EARNGO_DEVICE_ID: 1,
                    EARNGO_SESSION_TOKEN: 1,
                    EARNGO_ID: 1,
                    EARNGO_MOBILE_API_KEY: 1
                },
                data: {
                    init: "init"
                }
            };
            var promise = $http(req);

            return promise.then(function (payload) {
                console.log("payload : ", payload);
                //$scope.message = data;
                var sesTokenId = payload.data.EARNGO_DEVICE_ID;
                var deviceId = payload.data.EARNGO_SESSION_TOKEN;

                //set values for Session Token and DeviceId
                earngoStore.setSessionToken(sesTokenId);
                earngoStore.setDeviceId(deviceId);
            }).catch(function (error) {
                console.log('Assign only failure callback to promise', error);
                if (error) {
                    alertService.error('Error in Server. Please contact Support team.');
                }
                $q.reject({message: error});
            });
        }, //end of httpPost Init


        httpPostPromise: function (restURL, jsonReqData) {
            var req = {
                method: "post",
                url: restURL,
                headers: {
                    EARNGO_DEVICE_ID: earngoStore.getDeviceId(),
                    EARNGO_SESSION_TOKEN: earngoStore.getSessionToken(),
                    EARNGO_ID: earngoStore.getEarngoId(),
                    EARNGO_MOBILE_API_KEY: 1,
                },
                data: JSON.stringify(jsonReqData),
            };
            var promise = $http(req);
            return promise.then(function (payload) {
                if (payload.data && payload.data.RES_STATUS && payload.data.RES_STATUS === 'E') {
                    console.log("payload : ", payload);
                    alertService.error('Error in Server. Please contact Support team.');
                }
                return payload.data;
            }).catch(function (error) {
                console.log('Assign only failure callback to promise', error);
                if (error) {
                    alertService.error('Error in Server. Please contact Support team.');
                }
                // $q.reject({message: error});
                return {RES_STATUS: "E"}
            });

        }, //end of httpPost
    };
});