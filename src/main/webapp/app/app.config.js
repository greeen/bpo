/**
 * Created by Rajesh on 16/4/16.
 */
(function () {
    'use strict';

    var AppConfig = {
        INITIALIZE_DEVICE: 'service/login/device/init',
        ACTIVE_TASK_COUNT: 'service/login/task/active',
        VALIDATE_CUSTOMER: 'service/signin/validateCustomer',
        VALIDATE_TEMPLATE: 'service/forms/validateTemplate',
        REGISTER_TEMPLATE: 'service/forms/registerTemplate',
        FETCH_ALL_TEMPLATE_DETAIL: 'service/forms/details',
        FETCH_TEMPLATE_ROI: 'service/template/fetch/roi',
        SAVE_TEMPLATE_ROI: 'service/template/save/roi',
        FETCH_QUALITY_FORMS: 'service/quality/forms',
        FETCH_QUALITY_IMAGES: 'service/quality/images/all',
        FETCH_QUALITY_IMAGES_DATA: 'service/quality/imageData',
        FETCH_QUALITY_IMAGES_DATA_POST: 'service/quality/imagesDataPost',
        FETCH_FORMS_CATEGORY: 'service/forms/category',
        FETCH_USER_POINTS: 'service/profile/points/fetch',

        USER_LOGIN: 'service/login/validate',
        FAILED_IMAGE_DETAIL: 'service/failedForm/details',
        POST_FAILED_IMAGE_DETAIL: 'service/failedForm/save',
        POST_MATCH_FORM_DETAIL: 'service/failedForm/match/save',
        NEXT_MATCH_FORM_DETAIL: 'service/failedForm/match/details',

        URGENT_DATA_ON_PAGE_LOAD: 'service/nextImage/pageLoad',
        URGENT_DATA_AFTER_SUBMIT: 'service/nextImage/afterSubmit',
        LARGE_DATA_ON_PAGE_LOAD: 'service/nextImage/large/pageLoad',
        LARGE_DATA_AFTER_SUBMIT: 'service/nextImage/large/afterSubmit',
        POST_IMAGE_RESULT: 'service/nextImage/save',

        CHK_URGENT_DATA_ON_PAGE_LOAD : 'service/checker/nextImage/pageLoad',
        CHK_URGENT_DATA_AFTER_SUBMIT : 'service/checker/nextImage/afterSubmit',

        CHK_LARGE_DATA_ON_PAGE_LOAD : 'service/checker/nextImage/large/pageLoad',
        CHK_LARGE_DATA_AFTER_SUBMIT : 'service/checker/nextImage/large/afterSubmit',

        FETCH_BLANK_CLIPS : 'service/blank/clips/fetch',
        SAVE_BLANK_CLIPS : 'service/blank/clips/save'

    };

    var LOGIN_CONFIG =
    {
        DEVELOPMENT: {
            MOBILE_API_URL: "http://localhost:8080/homeweb/",
            FACEBOOK_APP_ID: '1747570878864994',
            GOOGLE_APP_ID: '685560997902-ha8lisqoqlsqdirk2vmu1dumus6hhioj.apps.googleusercontent.com',
        },
        PRODUCTION: {
            MOBILE_API_URL: "https://app.earnongo.com/home-api/",
            FACEBOOK_APP_ID: '1522043561417728',
            GOOGLE_APP_ID: '685560997902-ha8lisqoqlsqdirk2vmu1dumus6hhioj.apps.googleusercontent.com',
        }
    };

    var bpoConstants = {
        INITIALIZE_DEVICE: 'service/initDevice',
        ACTIVE_TASK_COUNT: 'service/activeTaskBPO',
        SIGNIN_REST_URL: 'service/signinRestCall',

        FAILED_IMAGE_DETAIL: 'service/failedImageDetails',
        POST_FAILED_IMAGE_DETAIL: 'service/postFailedImageDetails',
        POST_MATCH_FORM_DETAIL: 'service/postMatchFormDetails',
        NEXT_MATCH_FORM_DETAIL: 'service/nextMatchFormDetails'
    };

    //only for testing
    var devAppConfig = {
        INITIALIZE_DEVICE: 'http://app.earngo.com/customer/service/signin/initDevice',
        ACTIVE_TASK_COUNT: 'http://app.earngo.com/customer/service/activeTaskBPO',
        VALIDATE_CUSTOMER: 'http://app.earngo.com/customer/service/signin/validateCustomer',
        VALIDATE_TEMPLATE: 'http://app.earngo.com/customer/service/forms/validateTemplate',
        REGISTER_TEMPLATE: 'http://app.earngo.com/customer/service/forms/registerTemplate',
        FETCH_ALL_TEMPLATE_DETAIL: 'http://app.earngo.com/customer/service/forms/details',
        FETCH_TEMPLATE_ROI: 'http://app.earngo.com/customer/service/template/fetch/roi',
        SAVE_TEMPLATE_ROI: 'http://app.earngo.com/customer/service/template/save/roi'
    };

    var ClipTypeEnum = {
        SHORT_TEXT: "ST",
        SINGLE_SELECT_VERT: "MSV", // RADIO BUTTON
        SINGLE_SELECT_HOR: "MSH", // RADIO BUTTON
        MULTI_SELECT_VERT: "MMV",  //Checkbox
        MULTI_SELECT_HOR: "MMH",   //check boxes
        PHONE_NUMBER: "PH",
        NUMBER: "NM",
        DATE: "DT",
        TIME: "TM",
        DATE_AND_TIME: "DM",
        SNS: "SNS", // RADIO BUTTON
        MALE_FEMALE: "MF", // RADIO BUTTON
        YES_NO: "YN", // RADIO BUTTON
        LARGE_CLIP: "XL",    //text area
        LARGE_MCQ: "XLMMV",   //checkbox
    };

    var CLIP_TITLES = {
        SHORT_TEXT: "CLIPS.TITLE.SHORT_TEXT",
        SINGLE_SELECT_VERT: "CLIPS.TITLE.SINGLE_SELECT_VERT", // RADIO BUTTON
        SINGLE_SELECT_HOR: "CLIPS.TITLE.SINGLE_SELECT_HOR", // RADIO BUTTON
        MULTI_SELECT_VERT: "CLIPS.TITLE.MULTI_SELECT_VERT", // Checkbox
        MULTI_SELECT_HOR: "CLIPS.TITLE.MULTI_SELECT_HOR", // Checkbox
        PHONE_NUMBER: "CLIPS.TITLE.PHONE_NUMBER",
        NUMBER: "CLIPS.TITLE.NUMBER",
        DATE: "CLIPS.TITLE.DATE",
        TIME: "CLIPS.TITLE.TIME",
        DATE_AND_TIME: "CLIPS.TITLE.DATE_AND_TIME",
        SNS: "CLIPS.TITLE.SNS", // RADIO BUTTON
        MALE_FEMALE: "CLIPS.TITLE.MALE_FEMALE", // RADIO BUTTON
        YES_NO: "CLIPS.TITLE.YES_NO", // RADIO BUTTON
        LARGE_CLIP: "CLIPS.TITLE.LARGE_CLIP", // text area
        LARGE_MCQ: "CLIPS.TITLE.LARGE_MCQ" // checkbox
    };

    var CLIP_PATHS = {
        SHORT_TEXT: "assets/img/clips/short-text.jpg",
        SINGLE_SELECT_VERT: "assets/img/clips/mcq-single-vertical.jpg", // RADIO BUTTON
        SINGLE_SELECT_HOR: "assets/img/clips/mcq-single-horizontal.jpg", // RADIO BUTTON
        MULTI_SELECT_VERT: "assets/img/clips/mcq-multi-vertical.jpg",  //Checkbox
        MULTI_SELECT_HOR: "assets/img/clips/mcq-multi-horizontal.jpg",   //check boxes
        PHONE_NUMBER: "assets/img/clips/phone.jpg",
        NUMBER: "assets/img/clips/number.jpg",
        DATE: "assets/img/clips/date.jpg",
        TIME: "assets/img/clips/time.jpg",
        DATE_AND_TIME: "assets/img/clips/date-time.jpg",
        SNS: "assets/img/clips/selected-not-selected.jpg", // RADIO BUTTON
        MALE_FEMALE: "assets/img/clips/male-female.jpg", // RADIO BUTTON
        YES_NO: "assets/img/clips/yes-no.jpg", // RADIO BUTTON
        LARGE_CLIP: "assets/img/clips/large-text.jpg",    //text area
        LARGE_MCQ: "assets/img/clips/large-mcq-multi.jpg",   //checkbox
    };


    var CLIP_TYPE_DESC = [
        {name: 'Short Text', value: ClipTypeEnum.SHORT_TEXT},
        {name: 'Large Clip', value: ClipTypeEnum.LARGE_CLIP},
        {name: 'Number', value: ClipTypeEnum.NUMBER},
        {name: 'Phone Number', value: ClipTypeEnum.PHONE_NUMBER},
        {name: 'Selected / Not Selected', vvalue: ClipTypeEnum.SNS},
        {name: 'Date', value: ClipTypeEnum.DATE},
        {name: 'Time', value: ClipTypeEnum.TIME},
        {name: 'DateTime', value: ClipTypeEnum.DATE_AND_TIME},
        {name: 'Male Female', value: ClipTypeEnum.MALE_FEMALE},
        {name: 'Yes No', value: ClipTypeEnum.YES_NO},
        {name: 'Single Selection Vertical', value: ClipTypeEnum.SINGLE_SELECT_VERT},
        {name: 'Single Selection Horizontal', value: ClipTypeEnum.SINGLE_SELECT_HOR},
        {name: 'Single Selection Table', value: ClipTypeEnum.LARGE_MCQ},
        {name: 'Multiple Selection Vertical', value: ClipTypeEnum.MULTI_SELECT_VERT},
        {name: 'Multiple Selection Horizontal', value: ClipTypeEnum.MULTI_SELECT_HOR},
        {name: 'Multiple Selection Table', value: ClipTypeEnum.LARGE_MCQ},
    ];

    var QUALITY_TYPE = [
        {name: 'Single Entry', value: 1, default: false},
        {name: 'Double Entry', value: 2, default: true},
        {name: 'Triple Entry', value: 3, default: false},
    ];

    var SLA = [
        {name: '24 Hrs', value: 1440, default: true},
        {name: '8 Hrs', value: 480, default: false},
        {name: '4 Hrs', value: 160, default: false}
    ];

    var DEFAULT = {
        QUALITY_TYPE_VALUE: 2,
        SLA_TYPE_VALUE: 1440,
        CLIP_SUB_SEQ_NO: 1,
        LANGUAGE_SRNO: 1,
        CLIP_SEQ_NO: 1,
        CLIP_MANDATORY: 'N'
    };

    var LANGUAGE = [
        {name: 'English', value: 1},
        {name: 'Chinese', value: 2},
    ];

    var STATES_URLS = {
        login: {
            url: '/login'
        },
        'main.dashboard': {
            url: '/dashboard',
            title: "CUSTOMER.DASHBOARD.NAVIGATION.DASHBOARD.LABEL"
        },
        'main.forms': {
            url: '/forms',
            title: "CUSTOMER.DASHBOARD.NAVIGATION.FORMS.LABEL"
        },
        'main.templates': {
            url: '/templates',
            title: "CUSTOMER.DASHBOARD.NAVIGATION.TEMPLATES.LABEL"
        },
        'main.exception': {
            url: '/exception',
            title: "CUSTOMER.DASHBOARD.NAVIGATION.EXCEPTIONS.LABEL"
        },
        'main.quality': {
            url: '/quality',
            title: "CUSTOMER.DASHBOARD.NAVIGATION.QUALITY.LABEL"
        },
        'main.profile': {
            url: '/profile',
            title: "CUSTOMER.DASHBOARD.NAVIGATION.PROFILE.LABEL"
        },
        urgent: {
            url: '/clipUrgentEntry',
            title: "NAVIGATION.MENU_ITEM.URGENT_CLIPS",
            //itemId: "linkUrgentId",
            //linkId: "linkUrgentClip",
            href: "#clipUrgentEntry",
            //class: "",
            tooltip: "NAVIGATION.TOOLTIPS.URGENT_CLIPS"
        },
        large: {
            url: '/largeEntry',
            title: "NAVIGATION.MENU_ITEM.LARGE_CLIPS",
            //itemId: "liLargeId",
            //linkId: "liLargeId",
            href: "#largeEntry",
            //class: "",
            tooltip: "NAVIGATION.TOOLTIPS.LARGE_CLIPS"
        },
        matchForm: {
            url: "/matchform",
            title: "NAVIGATION.MENU_ITEM.LINK_FORMS",
            //itemId: "liLinkFormId",
            //linkId: "linkForm",
            href: "#matchform",
            //class: "",
            tooltip: "NAVIGATION.TOOLTIPS.LINK_FORMS"
        },
        formEntry: {
            url: "/formentry",
            title: "NAVIGATION.MENU_ITEM.ENTRY_FORMS",
            //itemId: "liEntryFormId",
            //linkId: "linkEntryForm",
            href: "#formentry",
            class: "",
            tooltip: "NAVIGATION.TOOLTIPS.ENTRY_FORMS"
        },
        home: {
            url: "/dashboard",
            title: "NAVIGATION.MENU_ITEM.DASHBOARD",
            ///itemId: "liEntryFormId",
            //linkId: "linkEntryForm",
            href: "#dashboard",
            //class: "",
            tooltip: "NAVIGATION.TOOLTIPS.DASHBOARD"
        },
        checkerUrgent: {
            url: "/checkerEntry",
            title: "NAVIGATION.MENU_ITEM.CHECKER_URGENT",
            //itemId: "liCheckerId",
            //linkId: "linkCheckerClip",
            href: "#checkerEntry",
            //class: "",
            tooltip: "NAVIGATION.TOOLTIPS.CHECKER_URGENT"
        },
        checkerLarge: {
            url: "/checkerLargeEntry",
            title: "NAVIGATION.MENU_ITEM.CHECKER_LARGE",
            //itemId: "liCheckerLargeId",
            //linkId: "linkCheckerLargeClip",
            href: "#checkerLargeEntry",
            //class: "",
            tooltip: "NAVIGATION.TOOLTIPS.LARGE_CLIPS"
        },
        profile: {
            url: "/profile",
            title: "NAVIGATION.MENU_ITEM.PROFILE",
            //itemId: "profile",
            //linkId: "profile",
            href: "#profile",
            //class: "",
            tooltip: "NAVIGATION.TOOLTIPS.PROFILE"
        },
        redeem: {
            url: "/redeem",
            title: "NAVIGATION.MENU_ITEM.REDEEM",
            ///itemId: "redeem",
            //linkId: "redeem",
            href: "#redeem",
            //class: "",
            tooltip: "NAVIGATION.TOOLTIPS.REDEEM"
        },
        blankClips: {
            url: '/blankClips',
            href: "#blankClips",
            title: "NAVIGATION.MENU_ITEM.BLANK_CLIPS",
            tooltip: "NAVIGATION.MENU_ITEM.BLANK_CLIPS"
        }
    };

    AppConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    angular.module('app.modules.config', [])
        .constant('LOGIN_CONFIG', LOGIN_CONFIG)
        .constant('CLIP_TYPE_DESC', CLIP_TYPE_DESC)
        .constant('QUALITY_TYPE', QUALITY_TYPE)
        .constant('SLA', SLA)
        .constant('DEFAULT', DEFAULT)
        .constant('LANGUAGE', LANGUAGE)
        .constant('config', AppConfig)
        .constant('bpoConstants', bpoConstants)
        .constant('clipTypeEnum', ClipTypeEnum)
        .constant('clipPaths', CLIP_PATHS)
        .constant('CLIP_TITLES', CLIP_TITLES)
        .constant('STATES_URLS', STATES_URLS);
})();
