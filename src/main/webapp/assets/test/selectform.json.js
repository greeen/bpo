var selectFormJson = {
  "RES_STATUS": "S",
  "RES_CODE": "101",
  "RES_MESSAGE": "NORMALISED PATH FOUND",
  "CATEGORY_DETAILS":[{
    'categoryId': 'cat1',
    'categoryName': 'Insurance '},
    {
      'categoryId': 'cat2',
      'categoryName': 'Application'}],
  "FORM_DETAILS": [{
    'categoryId': 'cat1',
    'categoryName': 'Insurance ',
    'forms': [{
      'formId': '1', 'formName': 'form One',
      'pages': [{
        'pageId': 'Page1',
        'pageName': 'Page One',
        'pageUrl': 'assets/img/canvas.jpg'
      },
        {
          'pageId': 'Page2',
          'pageName': 'Page Two',
          'pageUrl': 'assets/img/canvas.png'
        }]
    },
      {'formId': '2', 'formName': 'form Two'}
      , {'formId': '3', 'formName': 'Form Three'}]
  },
    {
      'categoryId': 'cat2',
      'categoryName': 'Application',
      'forms': [{'formId': '1', 'formName': 'App Form'},
        {'formId': '2', 'formName': 'App Form 2'}
        , {'formId': '3', 'formName': 'App Form 3'}]
    },
    {
      'categoryId': 'cat3',
      'categoryName': 'Loans',
      'forms': []
    }]
};

var registerTemplate = [{
  "CUST_SRNO": "1", "FORM_NAME": "122", "PAGE_NO": "1", "IMAGE_NAME": "2.jpg",
  "PAGE_LINKAGE": 0, "MP": "Y", "IMAGE_TYPE": "image/jpeg", "IMAGE_CATEGORY": "1"
},
  {
    "CUST_SRNO": "1", "FORM_NAME": "122", "PAGE_NO": "2", "IMAGE_NAME": "3.jpg",
    "PAGE_LINKAGE": 0, "MP": "Y", "IMAGE_TYPE": "image/jpeg", "IMAGE_CATEGORY": "1"
  }];

var zoneMarkedFormdReq = {"CUST_SRNO": "1", "TEMPLATE_ID": "CAG_2"};

var zoneMarkedFormRes1 = {
  "TEMPLATE_ID": "CAG_2",
  "RES_STATUS": "S",
  "RES_CODE": "101",
  "RES_MESSAGE": "NORMALISED PATH FOUND",
  "RES_IMAGE_PATH": "jpeg_normalised/1/CAG_1.jpg",
  "MP": "Y",
  "RES_IMAGE_HEIGHT": "2340",
  "RES_IMAGE_WIDTH": "1654",
  "TOTAL_ZONE_MARKINGS": "17",
  "FORM_NAME": "CAG",
  "FORM_ID": "1",
  "PAGE_LINKAGE": "1",
  "array": []
};
var zoneMarkedFormRes2 = {
  "TEMPLATE_ID": "CAG_1",
  "RES_STATUS": "S",
  "RES_CODE": "101",
  "RES_MESSAGE": "NORMALISED PATH FOUND",
  "RES_IMAGE_PATH": "jpeg_normalised/1/CAG_1.jpg",
  "MP": "Y",
  "RES_IMAGE_HEIGHT": "2340",
  "RES_IMAGE_WIDTH": "1654",
  "TOTAL_ZONE_MARKINGS": "17",
  "FORM_NAME": "CAG",
  "FORM_ID": "1",
  "PAGE_LINKAGE": "1",
  "array": [{
    "CLIP_COORDINATE_Y2": 2338,
    "CLIP_COORDINATE_X1": 3,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "NM",
    "CLIP_SEQ_NO": 1,
    "RANGE_VALUE_1": "0",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "form_no",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 2215,
    "CLIP_COORDINATE_X2": 488,
    "RANGE_VALUE_2": "101",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Enter number 1 to 100"
  }, {
    "CLIP_COORDINATE_Y2": 243,
    "CLIP_COORDINATE_X1": 11,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "NM",
    "CLIP_SEQ_NO": 2,
    "RANGE_VALUE_1": "0",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "QuarID",
    "CLIP_SLA": 86400,
    "CLIP_COORDINATE_Y1": 47,
    "CLIP_COORDINATE_X2": 547,
    "RANGE_VALUE_2": "101",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Enter number 1 to 100"
  }, {
    "CLIP_COORDINATE_Y2": 530,
    "CLIP_COORDINATE_X1": 1103,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "MSV",
    "CLIP_SEQ_NO": 3,
    "RANGE_VALUE_1": "",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "Sample",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 419,
    "CLIP_COORDINATE_X2": 1239,
    "RANGE_VALUE_2": "",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Choose options 1 or 2 only"
  }, {
    "CLIP_COORDINATE_Y2": 398,
    "CLIP_COORDINATE_X1": 275,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "ST",
    "CLIP_SEQ_NO": 4,
    "RANGE_VALUE_1": "",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "IntID",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 298,
    "CLIP_COORDINATE_X2": 635,
    "RANGE_VALUE_2": "",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "This field is case sensitive"
  }, {
    "CLIP_COORDINATE_Y2": 546,
    "CLIP_COORDINATE_X1": 280,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "ST",
    "CLIP_SEQ_NO": 5,
    "RANGE_VALUE_1": "",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "NAME",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 415,
    "CLIP_COORDINATE_X2": 799,
    "RANGE_VALUE_2": "",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "This field is case sensitive"
  }, {
    "CLIP_COORDINATE_Y2": 640,
    "CLIP_COORDINATE_X1": 182,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "NM",
    "CLIP_SEQ_NO": 6,
    "RANGE_VALUE_1": "0",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "Start_Hour",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 555,
    "CLIP_COORDINATE_X2": 355,
    "RANGE_VALUE_2": "25",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Enter number 1 to 24"
  }, {
    "CLIP_COORDINATE_Y2": 640,
    "CLIP_COORDINATE_X1": 336,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "NM",
    "CLIP_SEQ_NO": 7,
    "RANGE_VALUE_1": "0",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "Start_Min",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 551,
    "CLIP_COORDINATE_X2": 531,
    "RANGE_VALUE_2": "61",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Enter number 0 to 60"
  }, {
    "CLIP_COORDINATE_Y2": 1002,
    "CLIP_COORDINATE_X1": 48,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "MSV",
    "CLIP_SEQ_NO": 8,
    "RANGE_VALUE_1": "",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "S1",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 795,
    "CLIP_COORDINATE_X2": 207,
    "RANGE_VALUE_2": "",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Note: Sequence of options is wrong. 4 and 5 are misplaced "
  }, {
    "CLIP_COORDINATE_Y2": 1196,
    "CLIP_COORDINATE_X1": 218,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "NM",
    "CLIP_SEQ_NO": 9,
    "RANGE_VALUE_1": "17",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "S2",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 1091,
    "CLIP_COORDINATE_X2": 459,
    "RANGE_VALUE_2": "85",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Enter number 18 to 84"
  }, {
    "CLIP_COORDINATE_Y2": 1127,
    "CLIP_COORDINATE_X1": 896,
    "CLIP_DEST": "B",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "NM",
    "CLIP_SEQ_NO": 10,
    "RANGE_VALUE_1": "1",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "S3",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 791,
    "CLIP_COORDINATE_X2": 1043,
    "RANGE_VALUE_2": "99",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Enter number 1 to 6, 97, 98"
  }, {
    "CLIP_COORDINATE_Y2": 1287,
    "CLIP_COORDINATE_X1": 927,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "MF",
    "CLIP_SEQ_NO": 11,
    "RANGE_VALUE_1": "",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "S4",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 1183,
    "CLIP_COORDINATE_X2": 1098,
    "RANGE_VALUE_2": "",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": ""
  }, {
    "CLIP_COORDINATE_Y2": 1514,
    "CLIP_COORDINATE_X1": 126,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "YN",
    "CLIP_SEQ_NO": 12,
    "RANGE_VALUE_1": "",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "S5",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 1407,
    "CLIP_COORDINATE_X2": 264,
    "RANGE_VALUE_2": "",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": ""
  }, {
    "CLIP_COORDINATE_Y2": 1760,
    "CLIP_COORDINATE_X1": 64,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "MSV",
    "CLIP_SEQ_NO": 13,
    "RANGE_VALUE_1": "",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "S6",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 1552,
    "CLIP_COORDINATE_X2": 200,
    "RANGE_VALUE_2": "",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Choose 1 to 5"
  }, {
    "CLIP_COORDINATE_Y2": 1836,
    "CLIP_COORDINATE_X1": 176,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "ST",
    "CLIP_SEQ_NO": 14,
    "RANGE_VALUE_1": "",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "S6_others",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 1738,
    "CLIP_COORDINATE_X2": 576,
    "RANGE_VALUE_2": "",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "This field is case sensitive"
  }, {
    "CLIP_COORDINATE_Y2": 2094,
    "CLIP_COORDINATE_X1": 70,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "MSV",
    "CLIP_SEQ_NO": 15,
    "RANGE_VALUE_1": "",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "S7_a",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 1962,
    "CLIP_COORDINATE_X2": 195,
    "RANGE_VALUE_2": "",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Choose options 1 to 3"
  }, {
    "CLIP_COORDINATE_Y2": 1495,
    "CLIP_COORDINATE_X1": 870,
    "CLIP_DEST": "M",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "MSV",
    "CLIP_SEQ_NO": 16,
    "RANGE_VALUE_1": "",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "S7_b",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 1355,
    "CLIP_COORDINATE_X2": 1007,
    "RANGE_VALUE_2": "",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Choose option 4 to 6"
  }, {
    "CLIP_COORDINATE_Y2": 2123,
    "CLIP_COORDINATE_X1": 880,
    "CLIP_DEST": "B",
    "CLIP_QUALITY": 3,
    "CLIP_TYPE": "NM",
    "CLIP_SEQ_NO": 17,
    "RANGE_VALUE_1": "0",
    "LANGUAGE_SRNO": "1",
    "CLIP_FIELD_NAME": "S8",
    "CLIP_SLA": 14400,
    "CLIP_COORDINATE_Y1": 1622,
    "CLIP_COORDINATE_X2": 1040,
    "RANGE_VALUE_2": "100",
    "CLIP_SUB_SEQ_NO": 1,
    "CUSTOM_MESSAGE": "Enter Number 1 to 99"
  }]
};




/*Quality Page*/

var fetchFormDetail = {
  "array": [
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 1,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 32,
      "IMG_FORM_NAME": 'FORM A',
      "IMG_FORM_CAT": 'CAT A',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 97,
      "IMG_FORM_NAME": 'FORM B',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 37,
      "IMG_FORM_NAME": 'FORM C',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 27,
      "IMG_FORM_NAME": 'FORM D',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 17,
      "IMG_FORM_NAME": 'FORM E',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 23,
      "IMG_FORM_NAME": 'FORM F',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 87,
      "IMG_FORM_NAME": 'FORM G',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 11,
      "IMG_FORM_NAME": 'FORM H',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 21,
      "IMG_FORM_NAME": 'FORM I',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 41,
      "IMG_FORM_NAME": 'FORM J',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 43,
      "IMG_FORM_NAME": 'FORM K',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 34,
      "IMG_FORM_NAME": 'FORM L',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    },
    {
      "IMG_SUBMISSION_TIME": "2016-06-24 05:54:31",
      "IMG_NUM_COUNT": 6,
      "FORM_STATUS": "Y",
      "IMG_FORM_NUM": 45,
      "IMG_FORM_NAME": 'FORM M',
      "IMG_FORM_CAT": 'CAT B',
      "IMG_FORM_AGE": '48 hours'
    }
  ],
  "RES_STATUS": "S",
  "RES_CODE": "101",
  "RES_MESSAGE": "DATA FOUND"
};

var fetchImagesDetail =
    [
      {
        "IMG_BATCH_NUM": 0,
        "IMG_STATUS": "Y",
        "CUST_SRNO": 1,
        "IMG_NUM": 3,
        "IMG_NAME": "NA",
        "IMG_PATH": "assets/img/forma.jpg",
        "IMG_FORM_NUM": 0
      },
      {
        "IMG_BATCH_NUM": 0,
        "IMG_STATUS": "Y",
        "CUST_SRNO": 1,
        "IMG_NUM": 15,
        "IMG_NAME": "NA",
        "IMG_PATH": "assets/img/formb.jpg",
        "IMG_FORM_NUM": 0
      },
      {
        "IMG_BATCH_NUM": 0,
        "IMG_STATUS": "Y",
        "CUST_SRNO": 1,
        "IMG_NUM": 17,
        "IMG_NAME": "NA",
        "IMG_PATH": "assets/img/formc.jpg",
        "IMG_FORM_NUM": 0
      },
      {
        "IMG_BATCH_NUM": 0,
        "IMG_STATUS": "Y",
        "CUST_SRNO": 1,
        "IMG_NUM": 20,
        "IMG_NAME": "NA",
        "IMG_PATH": "assets/img/formd.jpg",
        "IMG_FORM_NUM": 0
      },
      {
        "IMG_BATCH_NUM": 0,
        "IMG_STATUS": "Y",
        "CUST_SRNO": 1,
        "IMG_NUM": 21,
        "IMG_NAME": "NA",
        "IMG_PATH": "assets/img/forme.jpg",
        "IMG_FORM_NUM": 0
      },
      {
        "IMG_BATCH_NUM": 0,
        "IMG_STATUS": "Y",
        "CUST_SRNO": 1,
        "IMG_NUM": 25,
        "IMG_NAME": "NA",
        "IMG_PATH": "assets/img/forma.jpg",
        "IMG_FORM_NUM": 0
      }
    ];

var qualityImgResponse =
    [
      { 
        "DIST_NUM": "47",
        "CLIP_FIELD_NAME": "form_no",
        "OUTTRAY_INPUT": "97",
        "CLIP_SUB_SEQ_NO": 1,
        "CLIP_SEQ_NO": 1
      },
      { 
        "DIST_NUM": "48",
        "CLIP_FIELD_NAME": "Q24",
        "OUTTRAY_INPUT": "N",
        "CLIP_SUB_SEQ_NO": 1,
        "CLIP_SEQ_NO": 3
      },
      { 
        "DIST_NUM": "49",
        "CLIP_FIELD_NAME": "S18",
        "OUTTRAY_INPUT": "1",
        "CLIP_SUB_SEQ_NO": 1,
        "CLIP_SEQ_NO": 4
      },
      { 
        "DIST_NUM": "50",
        "CLIP_FIELD_NAME": "S18_Others",
        "OUTTRAY_INPUT": "*",
        "CLIP_SUB_SEQ_NO": 1,
        "CLIP_SEQ_NO": 5
      },
      { 
        "DIST_NUM": "51",
        "CLIP_FIELD_NAME": "S19",
        "OUTTRAY_INPUT": "1",
        "CLIP_SUB_SEQ_NO": 1,
        "CLIP_SEQ_NO": 6
      },
      { 
        "DIST_NUM": "52",
        "CLIP_FIELD_NAME": "S19a",
        "OUTTRAY_INPUT": "6",
        "CLIP_SUB_SEQ_NO": 1,
        "CLIP_SEQ_NO": 7
      }
    ];

var qualityDataRes = {
  "RES_STATUS": "S",
  "RES_CODE": "100"
};


var blankTestData = {
  "data": [
    {
      "DIST_NUM": "1",
      "CLIP_PATH": "assets/img/clips/phone.jpg",
      "IMG_NUM": "1",
      "CUSTOM_MSG": "NA",
      "BLANK_STATUS": "Y",
      "CLIP_VALUE": ""
    },
    {
      "DIST_NUM": "2",
      "CLIP_PATH": "assets/img/clips/date.jpg",
      "IMG_NUM": "1",
      "CUSTOM_MSG": "NA",
      "BLANK_STATUS": "Y",
      "CLIP_VALUE": ""
    },
    {
      "DIST_NUM": "3",
      "CLIP_PATH": "assets/img/clips/date.jpg",
      "IMG_NUM": "1",
      "CUSTOM_MSG": "NA",
      "BLANK_STATUS": "Y",
      "CLIP_VALUE": ""
    },
    {
      "DIST_NUM": "4",
      "CLIP_PATH": "assets/img/clips/selected-not-selected.jpg",
      "IMG_NUM": "1",
      "CUSTOM_MSG": "NA",
      "BLANK_STATUS": "Y",
      "CLIP_VALUE": ""
    },
    {
      "DIST_NUM": "5",
      "CLIP_PATH": "assets/img/clips/yes-no.jpg",
      "IMG_NUM": "1",
      "CUSTOM_MSG": "NA",
      "BLANK_STATUS": "Y",
      "CLIP_VALUE": ""
    },
    {
      "DIST_NUM": "6",
      "CLIP_PATH": "assets/img/clips/date.jpg",
      "IMG_NUM": "1",
      "CUSTOM_MSG": "NA",
      "BLANK_STATUS": "Y",
      "CLIP_VALUE": ""
    }
  ],
  "RES_STATUS": "S",
  "RES_CODE": "101",
  "RES_MESSAGE": "DATA FOUND"
};