package com.earngo.exception.handler;

import com.earngo.exception.ConnectionFailure;
import com.earngo.exception.MuleFailureResponse;
import com.earngo.exception.UnknownFailureError;
import com.earngo.exception.model.ErrorDetail;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by rajeshkumarb on 05/07/2016.
 */
@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(MuleFailureResponse.class)
    public ResponseEntity<?> handleMuleFailureResponse(MuleFailureResponse failureResponse, HttpServletRequest request) {
        return new ResponseEntity<>(createErrorDetail(HttpStatus.INTERNAL_SERVER_ERROR, failureResponse),
                null,HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ConnectionFailure.class)
    public ResponseEntity<?> handleConnectionFailure(ConnectionFailure confailure, HttpServletRequest request) {
        return new ResponseEntity<>(createErrorDetail(HttpStatus.INTERNAL_SERVER_ERROR, confailure),
                null,HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UnknownFailureError.class)
    public ResponseEntity<?> handleUnknownFailureResponse(UnknownFailureError failureResponse, HttpServletRequest request) {
        return new ResponseEntity<>(createErrorDetail(HttpStatus.INTERNAL_SERVER_ERROR, failureResponse),
                null,HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ErrorDetail createErrorDetail(HttpStatus httpStatus,RuntimeException exception){
        ErrorDetail errorDetail = new ErrorDetail();
        errorDetail.setStatus("E");
        errorDetail.setCode(httpStatus.toString());
        errorDetail.setTitle(httpStatus.name());
        errorDetail.setMessage(exception.getMessage());
        //errorDetail.setDetail(exception.getMessage());
        return errorDetail;
    }
}
