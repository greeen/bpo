package com.earngo.exception.model;

import java.util.Calendar;

/**
 * Created by rajeshkumarb on 05/07/2016.
 */
public class ErrorDetail {
    private String title;
    private String status;
    private String code;
    private String detail;
    private long timeStamp;
    private String message;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public long getTimeStamp() {
        if (timeStamp == 0) {
            return Calendar.getInstance().getTimeInMillis();
        }
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
