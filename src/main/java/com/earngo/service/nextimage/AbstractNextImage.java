package com.earngo.service.nextimage;

import com.earngo.exception.UnknownFailureError;
import com.earngo.service.BaseRestService;
import com.earngo.util.CommonUtility;
import com.earngo.util.EarngoConstants;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rajesh on 16/7/16.
 */
public class AbstractNextImage extends BaseRestService {

    public JSONObject getImagesFromEarngo(MultivaluedMap<String, Object> headerMap, String requestStr, int noOfClips, boolean isPageLoad)
    throws UnknownFailureError{

        JSONObject jsonReqObj = new JSONObject(requestStr);
        if(jsonReqObj.get("SQS_NO")== null || jsonReqObj.get("SQS_NO").toString().equals("null")){
            throw new UnknownFailureError("Queue No is missing");
        }
        jsonReqObj.put("CUST_SRNO_TYPE", EarngoConstants.CUST_SRNO_TYPE);
        jsonReqObj.put("WRK_LANGUAGE_1", EarngoConstants.WRK_LANGUAGE_1);
        jsonReqObj.put("NO_OF_CLIPS", noOfClips);

        List<JSONObject> listOfObj1 = new ArrayList<JSONObject>();
        List<JSONObject> listOfObj2 = new ArrayList<JSONObject>();

        String response = restCall(headerMap, jsonReqObj.toString(), "GET_NEXT_CLIPS_URL");
        JSONObject json = new JSONObject(response);

        String status = json.getString("RES_STATUS");
        if (status.equalsIgnoreCase(EarngoConstants.BACKEND_ERROR_CODE) == true ||
                status.equalsIgnoreCase(EarngoConstants.BACKEND_FAILURE_CODE) == true) {
            json.put("imagesSetOne", CommonUtility.emptyJSONArray());
            json.put("imagesSetTwo", CommonUtility.emptyJSONArray());
            return json;
        }

        JSONArray clips = json.getJSONArray("CLIPS");
        divideJsonArray(isPageLoad, noOfClips, clips, listOfObj1, listOfObj2);

        json.put("imagesSetOne", listOfObj1);
        json.put("imagesSetTwo", listOfObj2);

        return json;
    }

    public JSONObject getLargeImagesFromEarngo(MultivaluedMap<String, Object> headerMap,String jsonStr, int noOfClips, boolean isPageLoad)
        throws UnknownFailureError{
        JSONObject jsonReqObj = new JSONObject(jsonStr);
        jsonReqObj.put("CUST_SRNO_TYPE", EarngoConstants.CUST_SRNO_TYPE);
        jsonReqObj.put("WRK_LANGUAGE_1", EarngoConstants.WRK_LANGUAGE_1);
        jsonReqObj.put("NO_OF_CLIPS", noOfClips);

        if(jsonReqObj.get("SQS_NO")== null || jsonReqObj.get("SQS_NO").toString().equals("null")){
            throw new UnknownFailureError("Queue No is missing");
        }
        List<JSONObject> listOfObj1 = new ArrayList<JSONObject>();
        List<JSONObject> listOfObj2 = new ArrayList<JSONObject>();
        String response = restCall(headerMap, jsonReqObj.toString(), "GET_NEXT_LARGE_IMAGE_URL");
        JSONObject json = new JSONObject(response);
        String status = json.getString("RES_STATUS");
        if (status.equalsIgnoreCase(EarngoConstants.BACKEND_ERROR_CODE) ||
                status.equalsIgnoreCase(EarngoConstants.BACKEND_FAILURE_CODE)) {
            json.put("imagesSetOne", CommonUtility.emptyJSONArray());
            json.put("imagesSetTwo", CommonUtility.emptyJSONArray());
            return json;
        }

        JSONArray clips = json.getJSONArray("CLIPS");
        divideJsonArray(isPageLoad, noOfClips, clips, listOfObj1, listOfObj2);

        json.put("imagesSetOne", listOfObj1);
        json.put("imagesSetTwo", listOfObj2);
        return json;
    }


    protected void divideJsonArray(boolean isPageLoad, int noOfClips, JSONArray clips, List<JSONObject> listOfObj1, List<JSONObject> listOfObj2)
            throws UnknownFailureError{
        for (int i = 0; i < clips.length(); i++) {
            JSONObject clipObj = clips.getJSONObject(i);
            JSONObject obj = new JSONObject();
            obj.put("clipNo", clipObj.get("DISTRIBUTOR_NUM"));
            obj.put("clipType", clipObj.get("INTRAY_CLIP_TYPE"));
            obj.put("clipPath", clipObj.get("INTRAY_CLIP_PATH"));
            obj.put("clipValue", CommonUtility.emptyString()); //set to empty value
            obj.put("customMsg", clipObj.get("CUSTOM_MESSAGE"));

            // here Value 3 was fixed for divide the json array
            if (isPageLoad == true) {
                if (i < noOfClips / 2) {
                    listOfObj1.add(obj);
                } else {
                    listOfObj2.add(obj);
                }
            } else {
                listOfObj1.add(obj);
                listOfObj2.add(obj);
            }
        } //end of loops
    }
}
