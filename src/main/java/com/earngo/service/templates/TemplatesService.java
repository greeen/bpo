package com.earngo.service.templates;

import com.earngo.service.BaseRestService;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;

/**
 * Created by Rajesh on 15/6/16.
 */
@Service
public class TemplatesService extends BaseRestService {

    public String fetchROI(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        return restCall(headerMap, uiReqJson, "FETCH_TEMPLATE_ROI");
    }

    public String saveROI(MultivaluedMap<String, Object> headerMap, String uiReqJson) {
        return restCall(headerMap, uiReqJson, "SAVE_TEMPLATE_ROI");
    }
}
