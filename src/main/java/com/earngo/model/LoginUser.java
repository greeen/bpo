package com.earngo.model;

import javax.validation.constraints.Size;

/**
 * Created by admin on 25/7/16.
 */
public class LoginUser {

    private String deviceId;

    private String password;

    private String sessionToken;

    @Size(min = 8, max = 20)
    private String username;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
