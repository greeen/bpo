package com.earngo.controller.base;

import com.earngo.util.EarngoConstants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rajesh on 21/5/16.
 */
public class BaseController {

    private Logger logger = Logger.getLogger(BaseController.class);

    protected static final String HEADER_APP_JSON = "Accept=application/json";

    @Autowired
    protected HttpServletRequest httpRequest;

    // get request headers
    protected Map<String, String> getHeadersInfo() {

        Map<String, String> map = new HashMap<String, String>();

        Enumeration<String> headerNames = httpRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = httpRequest.getHeader(key);
            map.put(key, value);
        }
        return map;
    }

    protected String getEarngoID() {
        String id = httpRequest.getHeader("earngo_id");
        if (id == null || id.equals("")) {
            id = httpRequest.getHeader("EARNGO_ID");
        }
        return id;
    }

    protected String getEarngoSessionToken() {
        return httpRequest.getHeader("EARNGO_ID");
    }

    protected String getEarngoDeviceId() {
        return httpRequest.getHeader("EARNGO_ID");
    }

    protected MultivaluedMap<String, Object> getEarngoHeader() {

        MultivaluedMap<String, Object> map = new MultivaluedHashMap<String, Object>();
        map.add(EarngoConstants.EARNGO_ID, httpRequest.getHeader("earngo_id"));
        map.add(EarngoConstants.SESSION_TOKEN_VALUE, httpRequest.getHeader("earngo_session_token"));
        map.add(EarngoConstants.DEVICE_ID, httpRequest.getHeader("earngo_device_id"));
        map.add(EarngoConstants.DEVICE, EarngoConstants.DEVICE_BPO);
        logger.debug("Header Values : " + map.toString());
        return map;
    }
}
