package com.earngo.controller;

import com.earngo.controller.base.BaseController;
import com.earngo.service.forms.FormsService;
import com.earngo.service.forms.FormsUploadService;
import com.earngo.util.ResponseMsg;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Created by Rajesh on 16/5/16.
 */

@RestController
@RequestMapping(value = "/service/forms")
public class FormsController extends BaseController {


    private Logger logger = Logger.getLogger(FormsController.class);

    @Autowired
    private FormsUploadService formsUploadService;
    @Autowired
    private FormsService formsService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/upload")
    public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file) throws IOException {
        ResponseMsg msg = formsUploadService.uploadFileHandler(file);
        return new ResponseEntity<String>(msg.getResponseJsonMsg(), HttpStatus.OK);
    }

    /**
     * Upload multiple file using Spring Controller
     */
    @RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
    public ResponseEntity<String> uploadMultipleFileHandler(@RequestParam("file") MultipartFile[] files) {
        ResponseMsg msg = formsUploadService.uploadMultipleFileHandler(files);
        return new ResponseEntity<String>(msg.getResponseJsonMsg(), HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test() throws IOException {
        System.out.println("This is my test form");
        return "Forms Test";
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/validateTemplate", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> validateUserLogin(@RequestBody String jsonStr) {
        logger.debug("validateUserLogin  : " + jsonStr);
        String result = formsService.validateTemplate(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/registerTemplate", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> registerTemplate(@RequestBody String jsonStr) {
        logger.debug("validateUserLogin  : " + jsonStr);
        String result = formsService.registerTemplate(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/details", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> fetchTemplateDetails(@RequestBody String jsonStr) {
        logger.debug("validateUserLogin  : " + jsonStr);
        String result = formsService.fetchFormsDetails(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }
}
