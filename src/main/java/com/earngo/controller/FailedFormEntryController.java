package com.earngo.controller;

import com.earngo.controller.base.BaseController;
import com.earngo.service.failedform.FailedFormEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/service/failedForm")
public class FailedFormEntryController extends BaseController {

    @Autowired
    private FailedFormEntryService failedFormEntryService;

    @RequestMapping(value = "/details", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getFailureImagesDetails(@RequestBody String jsonStr) {
        String result = failedFormEntryService.getImageDetails(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> postFailureImagesDetails(@RequestBody String jsonStr) {
        String result = failedFormEntryService.saveFailedFormData(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    //Below services for Match-Form
    @RequestMapping(value = "/match/details", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> getNextMatchFormDetails(@RequestBody String jsonStr) {
        String result = failedFormEntryService.getNextMatchFormDetails(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/match/save", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> postMatchFormDetails(@RequestBody String jsonStr) {
        String result = failedFormEntryService.saveMatchFormDetails(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

}