package com.earngo.controller;

import com.earngo.controller.base.BaseController;
import com.earngo.service.templates.TemplatesService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Rajesh on 15/6/16.
 */


@RestController
@RequestMapping(value = "/service/template")
public class TemplatesController extends BaseController {

    private Logger logger = Logger.getLogger(FormsController.class);

    @Autowired
    private TemplatesService templatesService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/fetch/roi", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> fetchROI(@RequestBody String jsonStr) {
        String result = templatesService.fetchROI(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/save/roi", method = RequestMethod.POST, headers = HEADER_APP_JSON)
    public ResponseEntity<String> saveROI(@RequestBody String jsonStr) {
        String result = templatesService.saveROI(getEarngoHeader(), jsonStr);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }
}
