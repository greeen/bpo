package com.earngo.util;

import com.google.gson.Gson;

/**
 * Created by Rajesh on 17/5/16.
 */
public class ResponseMsg {
    private String status;
    private String description;
    private String message;
    private Object data;


    public ResponseMsg() {
    }

    public String getResponseJsonMsg(){
        return new Gson().toJson(this);
    }

    public ResponseMsg(String status, String desc, String message) {
        this.status = status;
        this.description = desc;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public ResponseMsg setData(Object data) {
        this.data = data;
        return this;
    }
}
