package com.earngo.util;

/**
 * Created by Rajesh on 21/5/16.
 */
public class EarngoConstants {

    public static String EARNGO_ID="EARNGO_ID";
    public static String DEVICE_ID="EARNGO_DEVICE_ID";

    public static String DEVICE="EARNGO_DEVICE_TYPE";
    public static String DEVICE_BPO="EARNGO_DEVICE_BPO";
    public static String SESSION_TOKEN_VALUE="EARNGO_SESSION_TOKEN";

    public static String EARNGO_CUSTOMER_URL=   "/customer/";
    public static String EARNGO_UNKNOWWN_IMAGES_URL=   "/bpo/unknown_images/";

    public static String BPO_LARGE_IMAGE="BLI";
    public static String BPO_NORMAL_IMAGE="BNI";

    public static String BPO_TYPE="P";
    public static String HOME_TYPE="M";
    public static String CHECKER_TYPE="C";


    public static String WRK_LANGUAGE_1="1";
    public static String CUST_SRNO_TYPE="P";
    public static String OUT_TRAY_ACTION_UNCLEAR = "2" ;
    public static String OUT_TRAY_ACTION_VALUE = "1" ;
    public static String UNCLEAR_VALUE = "*" ;
    public static int NO_OF_CLIPS_AFTER_SUBMIT = 3;
    public static int NO_OF_CLIPS_ON_PAGE_LOAD = 6;
    public static int NO_OF_LARGE_CLIPS_ON_PAGE_LOAD = 2;
    public static int NO_OF_LARGE_CLIPS_AFTER_SUBMIT = 1;

    public static String BACKEND_ERROR_CODE = "E";
    public static String BACKEND_FAILURE_CODE = "F";



}
