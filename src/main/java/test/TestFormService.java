package test;

import com.earngo.service.forms.FormsService;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Rajesh on 14/6/16.
 */
public class TestFormService {

    public static void main(String args[]) {
        HashMap<String, String> map = new HashMap<>();
        // HashMap<String,String>[] mapArr= new HashMap[10];

        JSONArray mapArr = new JSONArray();

        for (int i = 0; i < 1; i++) {
            map = new HashMap<>();
            map.put("CATEGORY_ID", "1");
            map.put("CATEGORY_NAME", "Insurance");
            map.put("FORM_ID", "1");
            map.put("FORM_NAME", "form One");
            map.put("TEMPLATE_ID", "1" + i);
            map.put("TEMPLATE_NAME", "Tempalate_1" + i);
            map.put("TEMPLATE_URL", "assets/img/canvas.jpg");
            mapArr.put(map);

        }

        for (int i = 20; i < 22; i++) {
            map = new HashMap<>();
            map.put("CATEGORY_ID", "1");
            map.put("CATEGORY_NAME", "Insurance");
            map.put("FORM_ID", "2");
            map.put("FORM_NAME", "form One");
            map.put("TEMPLATE_ID", "1" + i);
            map.put("TEMPLATE_NAME", "Tempalate_1" + i);
            map.put("TEMPLATE_URL", "assets/img/canvas.jpg");
            mapArr.put(map);

        }


        for (int i = 40; i < 42; i++) {
            map = new HashMap<>();
            map.put("CATEGORY_ID", "1");
            map.put("CATEGORY_NAME", "Insurance");
            map.put("FORM_ID", "4");
            map.put("FORM_NAME", "form One");
            map.put("TEMPLATE_ID", "1" + i);
            map.put("TEMPLATE_NAME", "Tempalate_1" + i);
            map.put("TEMPLATE_URL", "assets/img/canvas.jpg");
            mapArr.put(map);

        }

        for (int i = 30; i < 32; i++) {
            map = new HashMap<>();
            map.put("CATEGORY_ID", "2");
            map.put("CATEGORY_NAME", "Insurance");
            map.put("FORM_ID", "3");
            map.put("FORM_NAME", "form One");
            map.put("TEMPLATE_ID", "1" + i);
            map.put("TEMPLATE_NAME", "Tempalate_1" + i);
            map.put("TEMPLATE_URL", "assets/img/canvas.jpg");
            mapArr.put(map);

        }


        JSONObject obj = new JSONObject(mapArr);
        obj.put("FORM_DETAILS", mapArr);


        System.out.println(mapArr.length());
        System.out.println(obj.toString());


       // new FormsService().transformForUI(obj.toString());
       // System.out.println(new FormsService().getAllCategory(obj.toString()));
    }
}
